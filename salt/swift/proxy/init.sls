include:
  - swift

swift_packages_proxy:
  pkg.installed:
    - names:
      - openstack-swift-proxy

memcached:
  pkg:
    - installed
  service.running:
    - enable: true

/etc/swift/proxy-server.conf:
  file.managed:
    - source: salt://swift/proxy/proxy-server.conf
    - template: jinja
    - context:
        memcache_servers: 127.0.0.1:11211
        storage_host: {{ grains.swift.proxy_listen_ip }}
    - require:
      - pkg: swift_packages_proxy

openstack-swift-proxy:
  service.running:
    - enable: true
    - watch:
      - file: /etc/swift/proxy-server.conf
    - require:
      - pkg: swift_packages_proxy
