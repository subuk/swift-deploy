base:
  '*':
    - common

  'role:swift.proxy':
    - match: grain
    - swift.proxy

  'role:swift.storage':
    - match: grain
    - swift.storage
