swift_packages_common:
  pkg.installed:
    - names:
      - openstack-swift

/etc/swift/swift.conf:
  file.managed:
    - user: root
    - group: swift
    - mode: '660'
    - source: salt://swift/swift.conf
    - template: jinja
    - context:
        swift_hash_path_suffix: 945e055d93862902945e055d93862902
        swift_hash_path_prefix: 077c576e084322f6077c576e084322f6
    - require:
      - pkg: swift_packages_common

/etc/swift/account.ring.gz:
  file.managed:
    - user: root
    - group: root
    - mode: '644'
    - source: salt://swift/rings/account.ring.gz
    - require:
      - pkg: swift_packages_common

/etc/swift/container.ring.gz:
  file.managed:
    - user: root
    - group: root
    - mode: '644'
    - source: salt://swift/rings/container.ring.gz
    - require:
      - pkg: swift_packages_common

/etc/swift/object.ring.gz:
  file.managed:
    - user: root
    - group: root
    - mode: '644'
    - source: salt://swift/rings/object.ring.gz
    - require:
      - pkg: swift_packages_common
