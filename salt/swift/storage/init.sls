include:
  - swift

swift_packages_storage:
  pkg.installed:
    - names:
      - openstack-swift-object
      - openstack-swift-container
      - openstack-swift-account
      - xfsprogs

rsync:
  pkg.installed:
    - names:
      - xinetd
      - rsync
  service.running:
    - name: xinetd
    - enable: true
    - watch:
      - file: /etc/xinetd.d/rsync

/etc/xinetd.d/rsync:
  file.managed:
    - source: salt://swift/storage/rsync.xinetd.conf
    - template: jinja
    - require:
      - pkg: xinetd
      - pkg: rsync

/etc/rsyncd.conf:
  file.managed:
    - source: salt://swift/storage/rsync.conf
    - template: jinja
    - context:
        listen_ip: {{ grains.swift.storage_listen_ip }}

openstack-swift-account:
  service.running:
    - enable: true
    - names:
      - openstack-swift-account
      - openstack-swift-account-auditor
      - openstack-swift-account-replicator
      - openstack-swift-account-reaper
    - watch:
      - file: /etc/swift/account-server.conf

openstack-swift-container:
  service.running:
    - enable: true
    - names:
      - openstack-swift-container
      - openstack-swift-container-auditor
      - openstack-swift-container-replicator
      - openstack-swift-container-updater
    - watch:
      - file: /etc/swift/container-server.conf

openstack-swift-object:
  service.running:
    - enable: true
    - names:
      - openstack-swift-object
      - openstack-swift-object-auditor
      - openstack-swift-object-replicator
      - openstack-swift-object-updater
    - watch:
      - file: /etc/swift/object-server.conf

/etc/swift/account-server.conf:
  file.managed:
    - source: salt://swift/storage/account-server.conf
    - template: jinja
    - context:
        listen_ip: {{ grains.swift.storage_listen_ip }}
    - require:
      - pkg: openstack-swift-account

/etc/swift/container-server.conf:
  file.managed:
    - source: salt://swift/storage/container-server.conf
    - template: jinja
    - context:
        listen_ip: {{ grains.swift.storage_listen_ip }}
    - require:
      - pkg: openstack-swift-container

/etc/swift/object-server.conf:
  file.managed:
    - source: salt://swift/storage/object-server.conf
    - template: jinja
    - context:
        listen_ip: {{ grains.swift.storage_listen_ip }}
    - require:
      - pkg: openstack-swift-object

{% for device in grains.swift.devices %}
{% set device_short_name = device.split('/')[-1] %}
/srv/node/{{ device_short_name }}:
  mount.mounted:
    - device: {{ device }}
    - persist: true
    - mkmnt: true
    - fstype: xfs

set_permission_{{ device_short_name}}:
  file.directory:
    - name: /srv/node/{{ device_short_name }}
    - user: swift
    - mode: '750'
    - recurse:
      - user
    - require:
      - mount: /srv/node/{{ device_short_name }}
{% endfor %}
