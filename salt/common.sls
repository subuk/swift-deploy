
include:
  - epel

dmidecode:
  pkg:
    - installed

bash-completion:
  pkg:
    - installed

htop:
  pkg:
    - installed

mc:
  pkg:
    - installed

vim-enhanced:
  pkg:
    - installed

ipython:
  pkg:
    - installed
